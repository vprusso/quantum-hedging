# Semidefinite programs for quantum hedging framework:

MATLAB scripts that implement the semidefinite program used in the papers:

[MW12] A. Molina and J. Watrous, Hedging bets with correlated quantum strategies. [arxiv:1104.1140][1]

[AMR13] S. Arunachalam, A. Molina, and V. Russo, Quantum hedging in the presence of protocol errors [arXiv:1310.7954][2]

Tested with MATLAB 9.0.0 (R2016a)

Requires:

- CVX -- [www.cvxr.com](http://cvxr.com/cvx/)
- QETLAB -- [http://www.qetlab.com/Main_Page](http://www.qetlab.com/Main_Page)
 
[1]: http://arxiv.org/abs/1104.1140
[2]: http://arxiv.org/pdf/1310.7954.pdf

## Usage

See `Code/Examples` folder